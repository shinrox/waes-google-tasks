(function() {
  'use strict';

  angular
    .module('waesGoogleTasks')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope) {
    var auth2 = gapi.auth2, peopleApi = gapi.client.people;

    $log.debug('runBlock end');

    $rootScope.user = {};

    auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    updateSigninStatus(auth2.getAuthInstance().isSignedIn.get());

    function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          getUser();
        } else {
          $rootScope.user.isSignedIn = false;
          $rootScope.$broadcast('loggedOut');
        }
      }

    function getUser() {
      // Make an API call to the People API, and print the user's given name.
      peopleApi.people.get({
        resourceName: 'people/me'
      }).then(function(response) {
          $rootScope.user.isSignedIn = true;
          $rootScope.user.userName = response.result.names[0].givenName;
          $rootScope.$broadcast('loggedIn');
      }, function(reason) {
        console.log('Error: ' + reason.result.error.message);
      });
    }
  }

})();
