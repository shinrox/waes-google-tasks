describe('TaskController', function() {
    var $scope, $controller, vm, Service, $q, $timeout,
    gapiResponse = {
        result: {
            names: [{
                givenName: 'Jonatas'
            }],
            items: [
                {
                    id: 1
                }
            ]
        }
    }, $uibModal, 
    promiseMock = {
      then: function(fn) {
        return fn();
      }
    };

    beforeEach(module('waesGoogleTasks'));

    // jasmine entende services com underline
    beforeEach(inject(function($rootScope, _$controller_, _$q_, _Tasks_, _$timeout_, _$uibModal_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $timeout = _$timeout_;
        $uibModal = _$uibModal_;

        $q = _$q_;

        Service = _Tasks_;
        
        promises = serviceSpy(Service, $q);

        spyOn($uibModal, 'open').and.returnValue({result: promiseMock});

        vm = $controller('TasksController', {
            $rootScope: $scope,
            $scope: $scope,
            $uibModal: $uibModal
        });

    }));

    describe('Load tasks without logged user', function() {
        it('Should not load tasklist on init', function() {
          Service.tasklist.list.calls.reset();

          vm = $controller('TasksController', {
              $rootScope: {
                user: {
                  isSignedIn: false
                }
              },
              $scope: $scope,
          });

          expect(Service.tasklist.list).not.toHaveBeenCalled();
          
        });

    });

    describe('Load tasks with logged user', function() {
        it('Should load tasklist on init', function() {
          promises.tasklist.list.resolve(gapiResponse);
          $scope.$digest();

          expect(Service.tasklist.list).toHaveBeenCalled();
          
        });

        it('Should load tasks on init', function() {
          promises.tasklist.list.resolve(gapiResponse);
          promises.task.list.resolve(gapiResponse);
          $scope.$digest();

          expect(Service.tasklist.list).toHaveBeenCalled();
          expect(Service.task.list).toHaveBeenCalled();
          
        });

    });

    describe('vm.isCompleted', function () {
      it('should return false if not', function() {
          expect(vm.isCompleted({status: 'needsAction'})).toBeFalsy();
      });

      it('should return true if it is', function() {
          expect(vm.isCompleted({status: 'completed'})).toBe(true);
        });
    });

    describe('vm.isCompleted', function () {
      it('should return false if not', function() {
          expect(vm.isCompleted({status: 'needsAction'})).toBeFalsy();
      });

      it('should return true if it is', function() {
          expect(vm.isCompleted({status: 'completed'})).toBe(true);
        });
    });

    describe('vm.loadTasks', function () {
      it('should call service api', function() {
          vm.loadTasks();
          expect(Service.tasklist.list).toHaveBeenCalled();
      });
    });


    describe('vm.save', function () {
      it('should call service save', function() {
          vm.add();
          $scope.$digest();
          expect(Service.task.save).toHaveBeenCalled();
      });
    });

    describe('vm.remove', function () {
      it('should call service remove', function() {
          vm.remove({});
          $scope.$digest();
          expect(Service.task.remove).toHaveBeenCalled();
      });
    });

    describe('vm.details', function () {
      it('should call service get', function() {
          vm.details({});
          promises.task.get.resolve(gapiResponse);
          $scope.$digest();

          expect(Service.task.get).toHaveBeenCalled();

          expect($uibModal.open).toHaveBeenCalled();
      });
    });

    describe('vm.openList', function () {
      it('should open modal', function() {
          vm.openList({});
          $scope.$digest();
          expect($uibModal.open).toHaveBeenCalled();
      });
    });


    function serviceSpy(Service, $q) {
       var promises = {}, key;

       for (key in Service) {
           if (!Service.hasOwnProperty(key)) { continue; }

           promises[key] = {};
           
           if (typeof Service[key] === 'function') {
               createSpy(promises, Service, key);
           } else {
               iterateObject(Service[key], promises[key]);
           }
       }

       function iterateObject(Service, promise) {
           for (key in Service) {
               if (!Service.hasOwnProperty(key)) { continue; }

               promise[key] = {};

               if (typeof Service[key] === 'function') {
                   createSpy(promise, Service, key);
               } else {
                   iterateObject(Service[key], promise[key]);
               }
           }
       }

       function createSpy(promisesObj, ServiceObj, method) {
           promisesObj[method] = $q.defer();
           spyOn(ServiceObj, method).and.returnValue(promisesObj[method].promise);
       }

       return promises;
   }


});
