(function() {
    'use strict';

    angular.module('waesGoogleTasks')
    .service('Tasks', function service(gapiRequest) {
        var api = gapi.client.tasks;

        var service = {
            tasklist: {
                list: getLists,
                save: saveList,
                remove: removeList
            },
            task: {
                list: getTasks,
                get: getTaskDetails,
                save: saveTask,
                remove: removeTask
            }
        };

        return service;


        function getLists() {
            return sendListRequest(api.tasklists.list);
        }

        function saveList(tasklist) {
            var method = tasklist.id ? api.tasklists.update : api.tasklists.insert;
            return sendListRequest(method, tasklist);
        }

        function removeList(tasklist) {
            return sendListRequest(api.tasklists.delete, tasklist);
        }

        function getTasks(tasklist) {
            tasklist.tasklist = tasklist.id;
            return gapiRequest(api.tasks.list, tasklist);
        }

        function getTaskDetails(task) {
            return sendTaskRequest(api.tasks.get, task);
        }

        function saveTask(task) {
            var method = task.id ? api.tasks.update : api.tasks.insert;
            return sendTaskRequest(method, task);
        }

        function removeTask(task) {
            return sendTaskRequest(api.tasks.delete, task);
        }

        function sendTaskRequest(request, task) {
            task.tasklist = task.tasklist.id;
            if (task.id) {
                task.task = task.id;
            }

            return gapiRequest(request, task);
        }

        function sendListRequest(request, tasklist) {
            if (tasklist && tasklist.id) {
                tasklist.tasklist = tasklist.id;
            }

            return gapiRequest(request, tasklist);
        }        
    });


})();