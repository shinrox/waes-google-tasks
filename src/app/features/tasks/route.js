(function() {
    'use strict';

    angular
    .module('waesGoogleTasks')
    .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('tasks', {
            url: '/',
            templateUrl: 'app/features/tasks/template.html',
            controller: 'TasksController as vm'
        });

        $urlRouterProvider.otherwise('/');
    }
})();