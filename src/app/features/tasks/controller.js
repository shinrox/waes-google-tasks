(function() {
	'use strict';

	angular.module('waesGoogleTasks')
	.controller('TasksController', function controller($rootScope, $scope, $uibModal, Tasks, $timeout) {

        var vm = this;

        // publics
        vm.openList = function openList(tasklist) {
            var modal = $uibModal.open({
                templateUrl: 'app/components/task-list-details/template.html',
                controller: 'TaskListDetailsController as vm',
                resolve: {
                    tasklist: function () {
                        return tasklist;
                    }
                }
            });

            modal.result.then(function(removed) {
                getLists(removed && (tasklist && tasklist.id === vm.currentTaskList.id));
            });
        };

        vm.loadTasks = function loadTasks(list) {
            var promise = Tasks.task.list(list);
            
            vm.currentTaskList = list;

            promise.then(function(res) {
                vm.tasks = res.result.items;
            });

            return promise;
        };

        vm.save = (function () {
            var timer = 500, timeout = {}, save;

            save = function save(task) {
                var params = angular.copy(task);
                params.tasklist = vm.currentTaskList;
                
                if (!vm.isCompleted(task)) {
                    delete params.completed;
                }

                Tasks.task.save(params).then(function(res) {
                    angular.merge(task, res.result);
                });
            };

            return function(task) {
                $timeout.cancel(timeout[task.id]);
                timeout[task.id] = $timeout(function(){
                    save(task);
                }, timer);
            };
        }());

        vm.add = function add($index) {
            var promise, params = {
                kind: 'tasks#task',
                status: 'needsAction',
                tasklist: vm.currentTaskList,
                title: '',
                notes: ''
            };

            if ($index >= 0) {
                params.previous = vm.tasks[$index].id;
            }
            

            promise = Tasks.task.save(params);
            promise.then(function(res) {
                if (vm.tasks) {
                    vm.tasks.splice($index, 0, res.result);
                    focus($index + 1);
                } else {
                    vm.tasks = [res.result];
                }
            });

            return promise;
        };

        vm.keyUp = function keyUp($event, $index, $last) {
            var keyMap = {
                '13': enter,
                '38': goUp,
                '40': goDown
            };

            if ($event && keyMap[$event.keyCode]) {
                keyMap[$event.keyCode]($index, $last);
            }
        };

        vm.remove = function remove(task) {
            task.tasklist = vm.currentTaskList;

            Tasks.task.remove(task).then(refresh);
        };

        vm.details = function details(task) {
            task.tasklist = vm.currentTaskList;

            Tasks.task.get(task).then(function(res) {
                res.result.tasklist = vm.currentTaskList;
                openTask(res.result);
            });
        };

        vm.isCompleted = function isCompletes(task) {
            return task.status === 'completed';
        };

        // privates
        function openTask(task) {
            var modal = $uibModal.open({
                templateUrl: 'app/components/task-details/template.html',
                controller: 'TaskDetailsController as vm',
                resolve: {
                    task: function () {
                        return task;
                    }
                }
            });

            modal.result.then(refresh);
        }

        function refresh() {
            return vm.loadTasks(vm.currentTaskList);
        }

        function getLists(getTasks) {
            Tasks.tasklist.list().then(function(res) {
                vm.lists = res.result.items;

                if (vm.lists.length > 0 && getTasks) {
                    vm.loadTasks(vm.lists[0]);
                }
            });
        }

        function enter($index) {
            vm.add($index);
        }

        function goDown($index, $last) {
            if (!$last) {
                focus($index + 1);
            }
        }

        function goUp($index) {
            if ($index > 0) {
                focus($index - 1);
            }
        }

        function focus($index) {
            
            var el = document.getElementById(vm.tasks[$index].id);
            if (el) {
                el.focus();
            }
        }

        function init() {
            vm.filters = [
                {
                    text: 'All',
                    status: 'all'
                },
                {
                    text: 'Not completed',
                    status: 'needsAction'
                },
                {
                    text: 'Completed',
                    status: 'completed'
                }

            ];

            vm.currentFilter = vm.filters[0];
            vm.user = $rootScope.user;

            if (vm.user.isSignedIn) {
                getLists(true);
            } else {
                $scope.$on('loggedIn', function() {
                    getLists(true);
                });
            }

            $scope.$on('loggedOut', function() {
                $scope.$apply();
            });
        }



        init();


    }
    );


})();