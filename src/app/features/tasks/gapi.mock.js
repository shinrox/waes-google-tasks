(function() {
    var tasksMock = {
        list: promiseMock,
        insert: promiseMock,
        update: promiseMock,
        get: promiseMock,
        'delete': promiseMock
    };

    window.gapi = {
        load: function(modules, fn) {
            return fn();
        },
        auth2: {
            getAuthInstance: function() {
                return {
                    isSignedIn:  {
                        listen: function()  {
                        
                        },
                        get: function() {
                            return true;
                        }
                    }
                };
            }
        },
        client: {
            init: promiseMock,
            people: {
                people: {
                    get: promiseMock
                }
                
            },
            tasks : {
                tasklists: tasksMock,
                tasks: tasksMock
            }
        }
    };

    function promiseMock() {
        return {
            then: function(fn) {
                return fn({
                    result: {
                        names: [{
                            givenName: 'Jonatas'
                        }],
                        items: [
                            {
                                id: 1
                            }
                        ]
                    }
                });
            }
        };
    }

})();