(function() {
    'use strict';

    angular
    .module('waesGoogleTasks')
    .config(config);

    /** @ngInject */
    function config($logProvider, cfpLoadingBarProvider, $httpProvider) {
        var contentType = {
            "Content-type": "application/x-www-form-urlencoded"
        };

    // Enable log
    $logProvider.debugEnabled(true);

    $httpProvider.defaults.transformRequest = function(data) {
        return data ? $.param(data, true) : data;
    };

    $httpProvider.defaults.headers.post = contentType;
    $httpProvider.defaults.headers.put = contentType;
    $httpProvider.defaults.headers["delete"] = contentType;

    // Set options third-party lib
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 250;

}

})();
