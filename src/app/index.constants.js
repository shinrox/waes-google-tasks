(function() {
  'use strict';

  angular
    .module('waesGoogleTasks')
    .constant('moment', moment);

})();
