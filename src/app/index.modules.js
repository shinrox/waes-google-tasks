(function() {
	'use strict';
	
	angular.module('waesGoogleTasks.vendors', [
		'ngSanitize',
		'ui.router',
		'ui.bootstrap',
		'ngToast',
		'angular-loading-bar'
	]);

	angular.module('waesGoogleTasks', ['waesGoogleTasks.vendors']);

	$(document).ready(function() {

		function start() {
			// Initialize the JavaScript client library.
			gapi.client.init({
				'apiKey': 'AIzaSyChzvN2s-qkBdejAIuQMTJ8DL_WW0T0rGo',
				'discoveryDocs': [
					'https://people.googleapis.com/$discovery/rest?version=v1',
					'https://www.googleapis.com/discovery/v1/apis/tasks/v1/rest'
				],
				'clientId': '784114884269-hj96rdrqka4l22p0p4m3jn5en7rmp4a6.apps.googleusercontent.com',
				'scope': 'profile https://www.googleapis.com/auth/tasks',
			}).then(function() {

				// bootstrap angular only when api load end
				angular.bootstrap(document, ['waesGoogleTasks']);
			});
		}

		// Load the Google JavaScript client and auth2 library
		gapi.load('client:auth2', start);

		
	});
})();