(function() {
    'use strict';

    angular.module('waesGoogleTasks')
    .controller('TaskListDetailsController', function($uibModalInstance, tasklist, Tasks) {

        var vm = this;

        vm.ok = function () {
            Tasks.tasklist.save(vm.tasklist).then($uibModalInstance.close);
        
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.remove = function remove() {
            Tasks.tasklist.remove(vm.tasklist).then(function() {
                $uibModalInstance.close(true);
            });
        };

        function init() {
            vm.tasklist = angular.copy(tasklist);
        }

        init();

    });

})();