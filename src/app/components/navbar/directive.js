(function() {
  'use strict';

  angular.module("waesGoogleTasks").directive("navBar", function() {
    return {
        restrict: "EA",
        scope: {},
        replace: true,
        templateUrl: 'app/components/navbar/template.html',
        controller: 'NavBarCtrl as vm'
    };
});

})();
