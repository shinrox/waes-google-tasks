(function() {
    'use strict';

    angular.module('waesGoogleTasks')
    .service('NavbarService', function(gapiRequest) {
        var service = {
            logout: logout,
            login: login
        };

        return service;

        function getAuth() {
            return gapi.auth2.getAuthInstance();
        }

        function logout() {
            return getAuth().signOut();
        }

        function login() {
            return getAuth().signIn();
        }
    });

})();