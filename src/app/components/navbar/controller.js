(function() {
    'use strict';

    angular.module("waesGoogleTasks")
    .controller("NavBarCtrl", function($rootScope, $scope, $document, $uibModal, $state, NavbarService) {


        var vm = this;

        $document.on('scroll', function() {
            vm.headerClass = document.body.scrollTop > 30 ? 'small' : 'big';
            return $scope.$apply();
        });

        vm.headerClass = document.body.scrollTop > 30 ? 'small' : 'big';

        vm.login = function login() {
            NavbarService.login();
        };

        vm.logout = function logout() {
            NavbarService.logout();
        };

        $scope.$on('loggedIn', updateLogin);

        $scope.$on('loggedOut', updateLogin);

        function updateLogin() {
            $scope.$apply();
        }


        // init
        function init() {
            vm.user = $rootScope.user;
        }

        init();
    });

})();