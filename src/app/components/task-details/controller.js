(function() {
    'use strict';

    angular.module('waesGoogleTasks')
    .controller('TaskDetailsController', function($uibModalInstance, task, Tasks) {

        var vm = this;

        vm.ok = function () {
            if (vm.task.status === 'needsAction') {
                delete vm.task.completed;
            }

            Tasks.task.save(vm.task).then($uibModalInstance.close);
        
        };

        vm.openDatePicker = function openDatePicker() {
            vm.datepickerOpen = true;
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.remove = function remove() {
            Tasks.task.remove(vm.task).then($uibModalInstance.close);
        };

        
        // init
        function init() {
            vm.dateOptions = {
                showWeeks: false,
                dateDisabled: false,
                formatYear: 'yyyy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };
            vm.task = angular.copy(task);

            if (vm.task.due) {
                vm.task.due = moment(vm.task.due, 'YYYY-MM-DD').toDate();
            }
        }

        init();

    });

})();