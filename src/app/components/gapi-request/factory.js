(function() {
    'use strict';

    angular.module('waesGoogleTasks')
    .factory('gapiRequest', function($q, cfpLoadingBar, ngToast) {

        return function send(request, params) {
            var defer = $q.defer(), promise;
            cfpLoadingBar.start();

            promise = params ? request(params) : request(params) ;

            if (promise.then) {
                promise.then(function(res) {
                    defer.resolve(res);
                    cfpLoadingBar.complete();
                }, function(reason) {
                    ngToast.danger('Ops, unknown error');
                    defer.reject(reason);
                    cfpLoadingBar.complete();
                });
            } else {
                defer.resolve();
                cfpLoadingBar.complete();
            }

            return defer.promise;
        };
    });

})();