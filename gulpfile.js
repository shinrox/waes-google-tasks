'use strict';

var gulp   = require('gulp');
var gutil  = require('gulp-util');

// Basic options 
// ========================= 
var options = {
  mainAngularModule : 'waesGoogleTasks'
};
 
 
// Base build modules 
// ========================== 
options.modulesData = {
	server : {
    routes : {
      '/bower_components': 'bower_components'
    },
    isEnabled: true
  }
}

/*
  ==========================
  Read gulp files
  ==========================
*/
require('basebuild-angular')(options);


/*
  ==========================
  Default Task
  ==========================
*/
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});
