# README #

To run this application you need the following

* NodeJs
* bower and gulp

### How to run ###

* run `npm i && bower i` after cloning this repo
* run `gulp dev` inside cloned folder

### How to run test ###

* run `gulp test` inside cloned folder
